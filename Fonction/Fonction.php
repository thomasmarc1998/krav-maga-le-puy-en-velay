<?php
class PdoKravmaga
{   		
  	private static $serveur='mysql:host=localhost';
  	private static $bdd='dbname=kravmaga';   		
  	private static $user='root' ;    		
  	private static $mdp='' ;
  	private static $monPdo;
	private static $Kravmaga=null;  	
/*	                    
 * Constructeur privé, crée l'instance de PDO qui sera sollicitée
 * pour toutes les méthodes de la classe
 */				
	private function __construct()
	{
    	PdoKravmaga::$monPdo = new PDO(PdoKravmaga::$serveur.';'.PdoKravmaga::$bdd, PdoKravmaga::$user, PdoKravmaga::$mdp); 
		PdoKravmaga::$monPdo->query("SET CHARACTER SET utf8");
	}
	public function _destruct()
	{
		PdoKravmaga::$monPdo = null;
	}
/**
 * Fonction statique qui crée l'unique instance de la classe
 * Appel : $instancePdoKravmaga = PdoKravmaga::getPdoKravmaga();
 * @return l'unique objet de la classe PdoKravmaga
 */
	public  static function getPdoKravmaga()
	{
		if(PdoKravmaga::$Kravmaga==null)
		{
			PdoKravmaga::$Kravmaga= new PdoKravmaga();
		}
		return PdoKravmaga::$Kravmaga;  
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////GET
        public function getActualite()
        {
            $req = "select * from actualite inner join article on NumA = NumAct";
            $var= PdoKravmaga::$monPdo->query($req);
            $lesLignes = $var->fetchAll();
            $nbLignes = count($lesLignes);
            return $lesLignes;
        }
        public function getCoursHorraires($nomC)
        {
            $req = "SELECT cours.NumC, cours.NomC, cours.Tarrif FROM `cours` 
            INNER join courshorraire on cours.NumC=courshorraire.NumC 
            INNER JOIN horaire on courshorraire.NumH =horaire.NumH 
            INNER JOIN intervenant on courshorraire.NumI = intervenant.NumI
            where NomC LIKE '%$nomC%' group by cours.NumC ORDER BY NomC ASC ";
            $var= PdoKravmaga::$monPdo->query($req);
            $lesLignes = $var->fetchAll();
            $nbLignes = count($lesLignes);
            return $lesLignes;
        }
        public function getCoursHorraires1($nomC)
        {
            $req = "SELECT * FROM `cours` 
            INNER join courshorraire on cours.NumC=courshorraire.NumC 
            INNER JOIN horaire on courshorraire.NumH =horaire.NumH 
            INNER JOIN intervenant on courshorraire.NumI = intervenant.NumI
            where NomC LIKE '%$nomC%' ORDER BY NomC ASC ";
            $var= PdoKravmaga::$monPdo->query($req);
            $lesLignes = $var->fetchAll();
            $nbLignes = count($lesLignes);
            return $lesLignes;
        }
        public function  getCours()
        {
            $req = "SELECT * FROM `cours`" ;
            $var= PdoKravmaga::$monPdo->query($req);
            $lesLignes = $var->fetchAll();
            $nbLignes = count($lesLignes);
            return $lesLignes;

        }
        public function  getHoraire()
        {
            $req = "SELECT * FROM `horaire`" ;
            $var= PdoKravmaga::$monPdo->query($req);
            $lesLignes = $var->fetchAll();
            $nbLignes = count($lesLignes);
            return $lesLignes;

        }
        public function  getIntervenant()
        {
            $req = "SELECT * FROM `intervenant`" ;
            $var= PdoKravmaga::$monPdo->query($req);
            $lesLignes = $var->fetchAll();
            $nbLignes = count($lesLignes);
            return $lesLignes;

        }
        public function  getCHI()
        {
            $req = "SELECT * FROM `courshorraire`" ;
            $var= PdoKravmaga::$monPdo->query($req);
            $lesLignes = $var->fetchAll();
            $nbLignes = count($lesLignes);
            return $lesLignes;

        }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////Ajout
        public function ajoutAct($Titre, $Description,$NomImage)
        {
            $req = "insert into actualite (NumAct, Titre, Description,NomImage)
            values(null,'$Titre', '$Description','$NomImage')";
            PdoKravmaga::$monPdo->exec($req);
            echo $req;
        }
        public function ajoutA($Contenue)
        {
            $req = "insert into article (NumA,Contenue)
            values(null, '$Contenue')";
            PdoKravmaga::$monPdo->exec($req);
            echo $req;
        }
        public function ajoutCHI($NumH,$NumC,$NumI)
        {
            $req = "insert into courshorraire (NumH,NumC,NumI)
            values($NumH,$NumC,$NumI)";
            PdoKravmaga::$monPdo->exec($req);
            echo $req;
        }
        public function ajoutH($DebH,$FinH,$LieuH,$JourH)
        {
            $req = "insert into horaire (NumH,DebH,FinH,LieuH,JourH)
            values(null,'$DebH','$FinH','$LieuH','$JourH')";
            PdoKravmaga::$monPdo->exec($req);
            echo $req;
        }
        public function ajoutC($NomC,$Tarif)
        {
            $req = "insert into cours (NumC,NomC,Tarrif)
            values(null,'$NomC','$Tarif')";
            PdoKravmaga::$monPdo->exec($req);
            echo $req;
        }
        public function ajoutI($NomI,$PrenomI)
        {
            $req = "insert into intervenant (NumI,NomI,PrenomI)
            values(null,'$NomI','$PrenomI')";
            PdoKravmaga::$monPdo->exec($req);
            echo $req;
        }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////Sup
        public function supCHI($NumC,$NumH,$NumI)
        {
            $req = "delete from courshorraire where NumC='$NumC' and NumH='$NumH' and NumI='$NumI'";
            PdoKravmaga::$monPdo->exec($req);
            echo $req;
        }
        public function supC($NumC)
        {
            $req = "delete from cours where NumC='$NumC'";
            PdoKravmaga::$monPdo->exec($req);
            echo $req;
        }
        public function supH($NumH)
        {
            $req = "delete from horaire where NumH='$NumH'";
            PdoKravmaga::$monPdo->exec($req);
            echo $req;
        }
        public function supI($NumI)
        {
            $req = "delete from intervenant where NumI='$NumI'";
            PdoKravmaga::$monPdo->exec($req);
            echo $req;
        }
        public function supAct($NumAct)
        {
            $req = "delete from article where NumA='$NumAct'; delete from actualite where NumAct='$NumAct'";
            PdoKravmaga::$monPdo->exec($req);
            echo $req;
        }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////Maj
        public function majC($NumC,$NomC,$Tarrif)
        {
            $req= " update cours set NomC ='$NomC' , Tarrif = '$Tarrif'
			where NumC ='$NumC'";
            PdoKravmaga::$monPdo->exec($req);
            echo $req;
        }
        public function majH($NumH,$DebH,$FinH,$LieuH,$JourH)
        {
            $req= " update horaire set DebH ='$DebH' , FinH = '$FinH' , LieuH = '$LieuH' ,JourH = '$JourH'
			where NumH ='$NumH'";
            PdoKravmaga::$monPdo->exec($req);
            echo $req;
        }
        public function majI($NumI,$NomI,$PrenomI)
        {
            $req= " update intervenant set NomI ='$NomI' , PrenomI = '$PrenomI'
			where NumI ='$NumI'";
            PdoKravmaga::$monPdo->exec($req);
            echo $req;
        }
        public function majAct($NumAct,$Titre, $Description,$NomImage)
        {
            if(!empty($NomImage))
            {
                $req= " update actualite set Titre ='$Titre' ,Description ='$Description' , NomImage = '$NomImage'
                where NumAct ='$NumAct'";
            }
            else
            {
                $req= " update actualite set Titre ='$Titre' ,Description ='$Description'
                where NumAct ='$NumAct'";
            }
            PdoKravmaga::$monPdo->exec($req);
        }
        public function majA($NumA,$Contenue)
        {
            $req = "update plante set plante.NomPlante = plante.NomPlante,plante.PrixPlante = '$prixPlante'
                    where plante.NumPlante ='$numPlante'";
            PdoKravmaga::$monPdo->exec($req);
            echo  $req;
        }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////Divers
        public function connexion($Email,$Passe)
        {
            $req = "SELECT * FROM adherent WHERE EmailAd='$Email' AND PasseAd='$Passe'";
            //	echo $req;
            $var =PdoKravmaga::$monPdo->query($req);
            $laLigne = $var->fetch();
            return $laLigne;
        }
}
	/*public function ajoutNouvellePlante($nomPlante,$prixPlante,$AdresseImage,$NomImage)
	{
		$req = "insert into plante (NumPlante, NomPlante, PrixPlante)
		values(NULL,'$nomPlante','$prixPlante')";

		$req1 ="insert into image (NumImage, AdresseImage,NomImage)
		values(NULL,'$AdresseImage','$NomImage')";
		echo "<p style= 'background-color: black; color:white;'>".$req."</p>";
		echo "<p style= 'background-color: black; color:white;'>".$req1."</p>";
		PdoKravmaga::$monPdo->exec($req);
		PdoKravmaga::$monPdo->exec($req1);
	}
	public function majPlanteImage($numPlante,$nomImage,$adresseImage)
	{		
			$req= " update image set image.AdresseImage ='$adresseImage' , image.NomImage = '$nomImage'
			where image.NumImage =$numPlante";
			echo "<p style= 'background-color: black; color:white;'>".$req."</p>";
			PdoKravmaga::$monPdo->exec($req);		
	}
	public function majPlanteSansImage($numPlante,$nomPlante,$prixPlante)
	{		
			if(empty($nomPlante))
			{
				$req = "update plante set plante.NomPlante = plante.NomPlante,plante.PrixPlante = '$prixPlante'
				where plante.NumPlante ='$numPlante'";
				PdoKravmaga::$monPdo->exec($req);	
			}
			else if(empty($prixPlante))
			{
				$req = "update plante set plante.NomPlante = '$nomPlante' , plante.PrixPlante = plante.PrixPlante
				where plante.NumPlante ='$numPlante'";
				PdoKravmaga::$monPdo->exec($req);	
			}
			else
			{
				$req = "update plante set plante.NomPlante = '$nomPlante' , plante.PrixPlante = '$prixPlante'
				where plante.NumPlante ='$numPlante'";
				echo "<p style= 'background-color: black; color:white;'>".$req."</p>";
				PdoKravmaga::$monPdo->exec($req);		
			}
 			
	}
	public function supPlante($numPlante)
	{	
 	    $req = "delete from plante where plante.NumPlante='$numPlante'";
		$req1 ="delete from image where image.NumImage='$numPlante'";
		echo "<p style= 'background-color: black; color:white;'>".$req."</p>";
		echo "<p style= 'background-color: black; color:white;'>".$req1."</p>";
		PdoKravmaga::$monPdo->exec($req);
		PdoKravmaga::$monPdo->exec($req1);
	}
	public function toutSupPlante()
	{	$reponse = PdoKravmaga::$monPdo->query('SELECT * FROM plante INNER JOIN image on image.NumImage =plante.NumPlante');
 		while ($donnees = $reponse->fetch())
 		{
	 	    $req = "delete from plante where plante.NumPlante=".$donnees['NumPlante']."";
			$req1 ="delete from image where image.NumImage=".$donnees['NumPlante']."";
			echo "<p style= 'background-color: black; color:white;'>".$req."</p>";
			echo "<p style= 'background-color: black; color:white;'>".$req1."</p>";
			PdoKravmaga::$monPdo->exec($req);
			PdoKravmaga::$monPdo->exec($req1);
		}
	}
	public function ajoutNouvelleCarteNormal()
	{
		?>
		<?php  
		$reponse = PdoKravmaga::$monPdo->query('SELECT * FROM plante INNER JOIN image on image.NumImage =plante.NumPlante');
 		while ($donnees = $reponse->fetch())
		{
			if(isset($_SESSION['Privilege'])&&$_SESSION['Privilege']==1)
			{
				?>
				<div  style="width: 25%; height: 300px; margin: 3%; text-align: center; cursor: pointer;"   class="animatedParent text-dark" >
				  <div style="display: flex; justify-content: flex-end; background-color: rgba(255,255,255,0.5);" class=" animated growIn">
			  		<a  data-toggle='modal' data-whatever='".$donnees["numPlante"]."' data-target="#exampleModalLong<?php echo $donnees['NumPlante'] ?>"><img  src='../image/modifier.jpg' style='width: 25px; height: 25px; ' ></a>
					<a href="../controler/controler.php?page=controler&param=Suprimer_plante&donne=<?php echo $donnees['NumImage']; ?>" ><img src='../image/delete.png' style='width: 25px;'></a>
				  </div>
				  <div style="width: 100%; height: 75%;  background: url('<?php echo "../Achat/".$donnees['NomImage']; ?>'); background-size: 100% 300px; " class=' animated growIn ' >
					  <div class="body">  	
					  </div>
				  </div>
				  <div class="container animated growIn"  style="background-color: rgba(255,255,255,0.5)" >
				  	<h5>
				    	<?php 
						   echo $donnees['NomPlante'];
					    ?>	
				    </h5>
				    <p>
				    	<?php 
						   echo "<b>".$donnees['PrixPlante']."€ TTC</b><br>";
						   echo "<b>".$donnees['NumPlante']."</b><br>";
					    ?>	
				    </p> 	
				    <button class="btn my-2 my-sm-0" style=" background-color: #5cb85c; margin:auto;" name="valider" value="Valider">
					  Ajouter au panier
					</button>  
				  </div>
				</div>
				<div class="modal fade " id="exampleModalLong<?php echo $donnees['NumPlante'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class=" modal-content" 
				    style="background-color: #fefefe;background-color: rgba(0,0,0,0.6);color: white;padding: 0; width: 80%;box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);-webkit-animation-name: animatetop;-webkit-animation-duration: 0.4s;animation-name: animatetop;animation-duration: 0.5s; width:150%;margin-left: -15%;  ">
				      <div class="infoPlante-header">
				        <h5 class="modal-title" id="exampleModalLongTitle">Modifier</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>

				      <form enctype="multipart/form-data" method="POST" action="../controler/controler.php?page=controler&param=Modifier_plante" >
					      <div class="infoPlante-body">
					       		<div class="box bg-light text-secondary " style="width: 75%; margin: auto;">
									  <div class="form-group row">
										  <label for="text" class="col-sm-4 col-form-label">Nom de la plante*</label>
										  <div class="col-sm-8">
										 	 <input class="form-control" type="text" placeholder="Nom de la plante" name="majNom">
										  </div>
									   </div>
									   <div class="form-group row">
										  <label for="text" class="col-sm-4 col-form-label">Prix la plante*</label>
										  <div class="col-sm-8">
										 	 <input class="form-control" type="text" placeholder="Prix de la plante" name="majPrix">
										  </div>
									   </div>
									   <div class="form-group row">
									    <label for="exampleFormControlFile1" class="col-sm-4 col-form-label">choisir une photo </label>
									    <div class="col-sm-8">
									    	 <input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
									    	 <input type="hidden" name="id" value=" <?php echo $donnees['NumPlante']?>" />
										 	 <input type="file" class="form-control-file"  name="majImage">
										</div>  
								     </div>
								</div>
					      </div>
					      <div class="modal-footer">
					        <button type="submit" class="btn" style="width: 50%; background-color: #5cb85c; margin:auto;" name="valider" value="Valider">
					       		 OK
					    	</button>
					      </div>
				      </form>
				    </div>
				  </div>
				</div>	
				<?php
			}
			else
			{
				?>
				<div  style="width: 25%; height: 300px; margin: 3%; text-align: center; cursor: pointer;"   class="animatedParent text-dark" >
				  <div style="width: 100%; height: 75%;  background: url('<?php echo "../Achat/".$donnees['NomImage']; ?>'); background-size: 100% 300px; " class=' animated growIn ' >
					  <div class="body">  	
					  </div>
				  </div>
				  <div class="container animated growIn"  style="background-color: rgba(255,255,255,0.5)" >
				  	<h5>
				    	<?php 
						   echo $donnees['NomPlante'];
					    ?>	
				    </h5>
				    <p>
				    	<?php 
						   echo "<b>".$donnees['PrixPlante']."€ TTC</b><br>";
					    ?>	
				    </p> 	
				    <button class="btn my-2 my-sm-0" style=" background-color: #5cb85c; margin:auto;" name="valider" value="Valider">
					  Ajouter au panier
					</button>  
				  </div>
				</div>
			<?php 
			}
			
		}
	}
	public function recherhe($nomPlante)
	{
		if(isset($nomPlante) && !empty($nomPlante))
		{
			$reponse = PdoKravmaga::$monPdo->query("SELECT * FROM plante INNER JOIN image on image.NumImage =plante.NumPlante where NomPlante LIKE '%$nomPlante%' ORDER BY NumPlante Desc");
			if ($reponse->rowCount()>0) 
			{
				while ($donnees = $reponse->fetch())
				{
					?>
						<div  style="width: 25%; height: 300px; margin: 3%; text-align: center;  cursor: pointer;"   class="animatedParent text-dark" >
						  <div style="width: 100%; height: 75%;  background: url('<?php echo "../Achat/".$donnees['NomImage']; ?>'); background-size: 100% 300px;; " class=' animated growIn ' >
						  	<div class="body">
						    </div>
						  </div>
						  <div class="container animated growIn" style="background-color: rgba(255,255,255,0.5)";>
						  	<h5>
						    	<?php 
								   echo $donnees['NomPlante'];
							    ?>	
						    </h5>
						    <p>
						    	<?php 
								   echo "<b>".$donnees['PrixPlante']."€ TTC</b><br>";
								   echo "ID: <b>".$donnees['NumImage']."</b>";
							    ?>	
						    </p> 	
						  </div>
						</div>
					<?php 
				}
			}
			else
			{
				?>
				<h1 class="text-dark">Aucun Resultat!</h1>
				<?php
			}
		}
		else
		{
			?>
			<h1 class="text-dark">Aucun Resultat!</h1>
			<?php
		}

	}
	public function AjoutUtilisateur($Email,$Passe,$Confirmation,$Nom,$Prenom,$Societe,$Tel,$Civilite,$AdresseFacturation,$Adresse1,$Adresse2,$Adresse3,$Ville,$Pays,$CodePostal)
	{
		$req = "insert into utilisateur (NumUser, Email, Passe, Confirmation, Civilite, Nom, Prenom, Societe, Tel, AdresseFacturation, Adresse1, Adresse2, Adresse3, Ville, Pays, CodePostal,Privilege)
		values(null,'$Email','$Passe','$Confirmation','$Civilite','$Nom','$Prenom','$Societe','$Tel','$AdresseFacturation','$Adresse1','$Adresse2','$Adresse3','$Ville','$Pays','$CodePostal',0)";
		//echo $req;
		PdoKravmaga::$monPdo->exec($req);
	}
	public function SuprimerUtilisateur($NumUser)
	{
		$req = "DELETE FROM utilisateur where NumUser='$NumUser'";
		PdoKravmaga::$monPdo->exec($req);
	}
	public function ModifPrivilege($NumUser,$Privilege)
	{
		$req= " update utilisateur set utilisateur.Privilege='$Privilege'
		where utilisateur.NumUser =$NumUser";
		PdoKravmaga::$monPdo->exec($req);
		echo $req;
	}
	public function AjoutParagraphe($Contenu)
	{
		$req = "insert into paragraphe (NumParagraphe, Contenu)
		values('$NumParagraphe', '$Contenu')";
		PdoKravmaga::$monPdo->exec($req);
		echo $req;;
	}
	public function ModifParagraphe($NumParagraphe,$Contenu)
	{
		$req = " update paragraphe set Contenu='$Contenu'
		where NumParagraphe =$NumParagraphe";
		PdoKravmaga::$monPdo->exec($req);
		echo $req;;
	}*/




