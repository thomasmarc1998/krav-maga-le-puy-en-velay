<?php
    function TranfertImage()
    {
        $prenom_nomOrigine = $_FILES['Image']['name'];
        $elementsChemin = pathinfo($prenom_nomOrigine);
        $extensionFichier = $elementsChemin['extension'];
        $extensionsAutorisees = array("jpeg", "jpg", "gif","png");
        if (!(in_array($extensionFichier, $extensionsAutorisees)))
        {
            echo " <p style= 'background-color: black; color:white;'  >Le fichier n'a pas l'extension attendue</p>";
        }
        else
        {
            // Copie dans le repertoire du script avec un prenom_nom
            // incluant l'heure a la seconde pres
            $repertoireDestination = dirname(__FILE__)."/../Images/Actualite/";

            $prenom_nomDestination = "fichier_du_".date("YmdHis").".".$extensionFichier;

            if (move_uploaded_file($_FILES['Image']["tmp_name"],$repertoireDestination.$prenom_nomDestination))
            {
                echo "<p style= 'background-color: black; color:white'  >Le fichier temporaire ".$_FILES['Image']["tmp_name"].
                    " a été déplacé vers".$repertoireDestination.$prenom_nomDestination."</p>";
                return $prenom_nomDestination;
            }
            else
            {
                $erreur= "<p style= 'background-color: black; color:white' >Le fichier n'a pas été uploadé (trop gros ?) ou ".
                    "Le déplacement du fichier temporaire a échoué".
                    " vérifiez l'existence du répertoire ".$repertoireDestination."</p>";
                return $erreur;
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function Contact($Prenom_Nom,$email_Form, $message_Form,$sujet_Form,$tel)
    {
        $email_destination="localhostdarko@gmail.com";
        if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#",$email_destination)) // On filtre les serveurs qui rencontrent des bogues.
        {
            $passage_ligne = "\r\n";
        }
        else
        {
            $passage_ligne = "\n";
        }
        //=====Déclaration des messages au format texte et au format HTML.
        $message_html = "<html><head></head><body><i><b>Message envoyé depuis Kravamaga43.com par ".$email_Form."</b></i>
                         <h1>Information:</h1>
                         <p>Nom/prenom: $Prenom_Nom</p>
                         <p>adresse mail: $email_Form</p>
                         <p>téléphone: $tel</p>
                         <br>
                         <p>$message_Form</p>
                         </body></html>";
        //==========

        //=====Création de la boundary
        $boundary = "-----=".md5(rand());
        //==========

        //=====Création du header de l'e-mail.
        $header = "From:\"Contact@kravmaga43.com\"<$email_Form>".$passage_ligne;
        $header.= "Reply-to:\"Contact@kravmaga43.com\"<$email_Form>".$passage_ligne;
        $header.= "MIME-Version: 1.0".$passage_ligne;
        $header.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
        //==========
         
        //=====Création du message.
        $message = $passage_ligne."--".$boundary.$passage_ligne;
        //=====Ajout du message au format texte.
        $message.= "Content-Type: text/plain; charset=\"ISO-8859-1\"".$passage_ligne;
        $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
        //==========
        $message.= $passage_ligne."--".$boundary.$passage_ligne;
        //=====Ajout du message au format HTML
        $message.= "Content-Type: text/html; charset=\"ISO-8859-1\"".$passage_ligne;
        $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
        $message.= $passage_ligne.$message_html.$passage_ligne;
        //==========
        $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
        $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
        //==========
         
        //=====Envoi de l'e-mail.
        mail($email_destination,$sujet_Form,$message,$header);
    //==========
    }
?>