<body>
	<header>
		<!------------------------------------------------------------Menu---------------------------------------------------------------------------------->
		<nav class="navbar navbar-expand-lg navbar-light   bg-danger text-white">
			<button class="navbar-toggler boutonNav" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
		   	    <span class="navbar-toggler-icon"></span>
		    </button>
		    <div class="collapse navbar-collapse " id="navbarTogglerDemo01">
		    	<div class=" nav-bar m-0  flex-wrap d-flex flex-row justify-content-start" >
					<div class="nav-item dropdown surlignements">
						<div class="nav-link  "  id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					 		<img class ="Logos" src="Images/Logos/krav-maga_Logo.png">
						</div>
			  		</div>
                    <div class="nav-item dropdown surlignements ">
                        <div class="nav-link dropdown-toggle pointer"  onclick="location.href='index.php'" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Accueil
                        </div>
                    </div>
			  		<div class="nav-item dropdown surlignements ">
						<div class="nav-link dropdown-toggle pointer"  onclick="location.href='index.php?page=Controler&param=Cours'" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	 						Les Cours
						</div>
					</div>
					<div class="nav-item dropdown surlignements  ">
						<div class="nav-link dropdown-toggle pointer " href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	 						Le club
						</div>
	   					<div class="dropdown-menu " aria-labelledby="navbarDropdown">
	   						<p class="lienMenu " href="../Pages/dionea.php">Notre Club</p>
	   						<p class="lienMenu" href="../Pages/lesDroerseras.php">Enseingnant</p>
	   					</div>
					</div>
					<div class="nav-item dropdown surlignements">
						<div class="nav-link dropdown-toggle pointer " href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		 					Le krav maga
						</div>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<p class=" lienMenu" href="../Pages/dionea.php">Historique</p>
							</div>
					</div>
                    <div class="nav-item dropdown surlignements ">
                        <div class="nav-link dropdown-toggle pointer"  id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Fiche inscription
                        </div>
                        <div class="dropdown-menu" onclick="location.href='index.php?page=Controler&param=Fiche'" aria-labelledby="navbarDropdown">
                            <p class=" lienMenu" >Fiche</p>
                        </div>
                    </div>

					<div class="nav-item dropdown surlignements">
						<div class="nav-link dropdown-toggle pointer " href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Evenement
						</div>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown"></div>
					</div>
					<?php if((!isset($_SESSION['EmailAd'])&&empty($_SESSION['EmailAd']))&&(!isset($_SESSION['PasseAd'])&&empty($_SESSION['PasseAd'])))
						{
							?><button class="btn btn-outline-light "   onclick="location.href='index.php?page=Controler&param=Connexion';" >Connexion</button><?php
						}
					    if((isset($_SESSION['EmailAd'])&&!empty($_SESSION['EmailAd']))&&(isset($_SESSION['PasseAd'])&&!empty($_SESSION['PasseAd'])))
						{
							?><button class="btn btn-outline-light "   onclick="location.href='index.php?page=Controler&param=se-deconnecter';" >Deconexion <?php echo $_SESSION['PrenomAd']?></button><?php
						}
					?>
				</div>
				<div class="d-flex flex-row mr-0 flex-wrap justify-content-end w-50 align-self-baseline">
					<div class="nav-item dropdown surlignements">
						<span class=" nav-link  glyphicon glyphicon-earphone pointer "> 06.65.23.08.11</span>
					</div>
					<div class="nav-item dropdown surlignements">
						<span  class=" nav-link glyphicon glyphicon-envelope pointer"> kravmagahl@gmail.com</span>
					</div>
					<div class="nav-item dropdown surlignements">
						<a href="https://www.facebook.com/kravmagahauteloire/" class="fa fa-facebook nav-link surlignements ">&nbsp; Facebook</a>
					</div>
				</div>
		    </div>
		</nav>
		<!------------------------------------------------------------Caroussel---------------------------------------------------------------------------------->
		<div id="carouselExampleControls" class="carousel slide bg-dark" data-ride="carousel" style="z-index:1">
            <div class="triangle-right"></div>


	  	   <div class="carousel-inner">

		    <div class="carousel-item active">

		      	<div class="flex" style="background:url('Images/Slider/1')fixed;background-size: cover;height: 200px;">
                </div>
		    </div>
		    <div class="carousel-item">
		      <div class="flex" style="background:url('Images/Slider/2')fixed;background-size: cover;height: 200px">
			  </div>
		    </div>
		    <div class="carousel-item">
		       <div class="flex" style="background:url('Images/Slider/3')fixed;background-size: cover;height: 200px">
			   </div>
		    </div>
		  </div>
		  <a class="carousel-control-prev" style="z-index: 2" href="#carouselExampleControls" role="button" data-slide="prev">
		    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="carousel-control-next" style="z-index:1" href="#carouselExampleControls" role="button" data-slide="next">
		    <span class="carousel-control-next-icon" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		  </a>
		 </div>
	</header>