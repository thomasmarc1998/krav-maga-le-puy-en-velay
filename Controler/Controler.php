<?php

	require_once( dirname(__FILE__).'/../Fonction/Fonction.php');
	require_once( dirname(__FILE__).'/../Fonction/Outils.php');
	$Pdo = PdoKravmaga::getPdoKravmaga();
		/**********************************************************************************************************************************************************************************************/
		/*****************************************************************************Fonctionalité****************************************************************************************************/
		/**********************************************************************************************************************************************************************************************/
	if(isset($_REQUEST["param"]))
	{
		$param = $_REQUEST["param"];
		switch($param)
		{
			////////////////////////////////////////////////////////////////////////////////////////////////////////////Pages
			case 'Connexion':
			{
				include (dirname(__FILE__).'/../Vue/Connexion.php');
				break;
			}
			case 'Contact':
			{
				include (dirname(__FILE__).'/../Vue/Contact.php');
				break;
			}
			case 'Accueil':
			{
				$var=$Pdo->getActualite();
				include (dirname(__FILE__).'/../Vue/Accueil.php');
				break;
			}
			case 'Article':
			{
				$var=$Pdo->getActualite();
				include (dirname(__FILE__).'/../Vue/Article.php');
				break;
			}
            case 'Cours':
            {

                if((!isset($_SESSION['EmailAd'])&&empty($_SESSION['EmailAd']))&&(!isset($_SESSION['PasseAd'])&&empty($_SESSION['PasseAd'])))
                {
                    $var1=$Pdo->getCoursHorraires('');
                    foreach ($var1 as $ligne)
                    {
                        $var = $Pdo->getCoursHorraires1($ligne['NomC']);
                        include(dirname(__FILE__) . '/../Vue/Cours.php');
                    }
                }
                else
                {
                    $var=$Pdo->getCours();
                    $var1=$Pdo->getHoraire();
                    $var2=$Pdo->getIntervenant();
                    include(dirname(__FILE__) . '/../Vue/Cours.php');

                }
                break;
            }
            case 'Fiche':
                {
                    include (dirname(__FILE__).'/../Vue/Fiche.php');
                    break;
                }
            case 'AjoutCour':
                {
                    $var=$Pdo->getCours();
                    include (dirname(__FILE__).'/../Vue/AjoutCour.php');
                    break;
                }
            case 'AjoutHoraire':
                {
                    $var=$Pdo->getHoraire();
                    include (dirname(__FILE__).'/../Vue/AjoutHoraire.php');
                    break;
                }
            case 'AjoutIntervenant':
                {
                    $var=$Pdo->getIntervenant();
                    include (dirname(__FILE__).'/../Vue/AjoutIntervenant.php');
                    break;
                }
            case 'S_M_CHI':
                {
                    if((isset($_SESSION['EmailAd'])&&!empty($_SESSION['EmailAd']))&&(isset($_SESSION['PasseAd'])&&!empty($_SESSION['PasseAd'])))
                    {
                        $var1=$Pdo->getCoursHorraires('');
                        foreach ($var1 as $ligne)
                        {
                            $var = $Pdo->getCoursHorraires1($ligne['NomC']);
                            include(dirname(__FILE__) . '/../Vue/S_M_CHI.php');
                        }
                    }
                    break;
                }
			/**********************************************************************************************************************************************************************************************/
			/*****************************************************************************Fonctionalité****************************************************************************************************/
			/**********************************************************************************************************************************************************************************************/
			case 'ValiderConnexion' :
			{
				//on vérifie tout d'abord si les champs sont bien remplis
				if(!empty($_REQUEST['EmailAd'])&&!empty($_REQUEST['PasseAd']))
				{
					$email = htmlspecialchars($_REQUEST['EmailAd']);
					//$mdp = sha1($_REQUEST['PasseAd']);
					$laLigne=$Pdo->connexion($email,$mdp);
					//on vérifie ensuite si un compte est bien retourné de la bdd (si $laLigne == 0 c'est qu'aucun compte Membre n'est retourné et qu'il y a forcément une erreur dans les logs)
					if($laLigne != 0)
					{
						$_SESSION['NomAd'] = $laLigne['NomAd'];
						$_SESSION['EmailAd'] = $laLigne['EmailAd'];
						$_SESSION['PasseAd'] = $laLigne['PasseAd'];
						$_SESSION['PrenomAd'] = $laLigne['PrenomAd'];
						$_SESSION['DroitAd'] = $laLigne['DroitAd'];

						echo '<div class="container">';
						echo " <div class='p-3  bg-success text-white' style='text-align:center;font-size:50px; margin:200px;' role='alert'>";
						echo '  	Bonjour '.$_SESSION['PrenomAd'].' '.$_SESSION['NomAd'].'';
						echo ' </div>';
						echo '</div>';
						?>
						<script >
							  setTimeout("location.href = 'index.php?page=Controler&param=Connexion';", 1500);
						</script>
				<?php
					}
					else
					{
						echo '<div class="container">';
						echo " <div class='p-3  bg-danger text-white' style='text-align:center;font-size:50px; margin:200px;' role='alert'>";
						echo '  	<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>';
						echo '  	<span class="sr-only">Erreur:</span>';
						echo '  		Mot de passe ou login non correct ';
						echo ' </div>';
						echo '</div>';
					}
				}
				else
				 {
					echo '<div class="container">';
					echo " <div class='p-3  bg-danger text-white' style='text-align:center;font-size:50px; margin:200px;' role='alert'>";
					echo '  	<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>';
					echo '  	<span class="sr-only">Erreur:</span>';
					echo '  		les champs ne sont pas remplis ';
					echo ' </div>';
					echo '</div>';
				}
				?>
				<?php
				break;
			}
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			case 'Ajouter':
			{
				if(isset($_SESSION['DroitAd'])&&!empty($_SESSION['DroitAd'])&&$_SESSION['DroitAd']==1)
				{
					if(!empty($_FILES['Image']['name'])&& (!empty($_POST["Description"]) && !empty($_POST["Titre"])))
					{
					    $var=TranfertImage();
						$Pdo->ajoutAct(addslashes(htmlspecialchars($_POST['Titre'])), addslashes(htmlspecialchars($_POST['Description'])),$var);
						$Pdo->ajoutA("");
						?>
                            <script >
                                document.location.href="index.php?page=Controler&param=Accueil";
                            </script>
						<?php
					}
					else
					{
						?>
                            <script >
                                    document.location.href="controler.php?param=Erreur_Form";
                            </script>
						<?php
					}
				}
				else
				{
					echo "erreur";
				}
				break;
			}
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			case 'Modifier':
			{
				if(isset($_SESSION['DroitAd'])&&!empty($_SESSION['DroitAd'])&&$_SESSION['DroitAd']==1)
				{
					if(isset($_REQUEST["modif"]))
					{
						$param = $_REQUEST["modif"];
						switch($param)
						{
							case 'modifActu':
							{
								if(!empty($_POST["Description"]) && !empty($_POST["Titre"]))

								{
									$var=TranfertImage();
									$Pdo->majAct($_POST['NumAct'],addslashes(htmlspecialchars($_POST['Titre'])),addslashes(htmlspecialchars($_POST['Description'])),$var);
									?>
										<script >
											document.location.href="index.php?page=Controler&param=Accueil";
										</script>
									<?php
								}
								else
								{
									?>
									<script >
										   document.location.href="index.php?page=Controler&param=Accueil";
								    </script>
									<?php
								}
								break;
							}
                            case 'modifArticle':
                            {
                                $Pdo->majA($_POST['id'],addslashes(htmlspecialchars($_POST['Contenue'])));
                                break;
                            }
                            case 'majC':
                                {
                                    $Pdo->majC($_POST['NumC'],$_POST['NomC'],$_POST['Tarrif']);
                                    ?>
                                        <script >
                                            document.location.href="index.php?page=Controler&param=AjoutCour";
                                        </script>
                                    <?php
                                    break;
                                }
                            case 'majH':
                                {
                                    $Pdo->majH($_POST['NumH'],$_POST['DebH'],$_POST['FinH'],$_POST['LieuH'],$_POST['JourH']);
                                    ?>
                                    <script >
                                        document.location.href="index.php?page=Controler&param=AjoutHoraire";
                                    </script>
                                    <?php
                                    break;
                                }
                            case 'majI':
                                {
                                    $Pdo->majI($_POST['NumI'],$_POST['NomI'],$_POST['PrenomI']);
                                    ?>
                                    <script >
                                        document.location.href="index.php?page=Controler&param=AjoutIntervenant";
                                    </script>
                                    <?php
                                    break;
                                }
						}
					}
				}
				break;
			}
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			case 'suprimer':
			{
                if(isset($_SESSION['DroitAd'])&&!empty($_SESSION['DroitAd'])&&$_SESSION['DroitAd']==1)
                {
                    if(isset($_REQUEST["sup"]))
                    {
                        $param = $_REQUEST["sup"];
                        switch($param)
                        {
                            case 'supAct':
                                {
                                    $Pdo->supAct($_REQUEST['NumAct']);
                                    ?>
                                    <script >
                                        document.location.href="index.php?page=Controler&param=Accueil";  C
                                    </script>
                                    <?php
                                    break;
                                }
                            case 'supCHI':
                                {
                                    $Pdo->supCHI($_POST['NumC'],$_POST['NumH'],$_POST['NumI']);
                                    ?>
                                    <script >
                                        document.location.href="index.php?page=Controler&param=S_M_CHI";
                                    </script>
                                    <?php
                                    break;
                                }
                            case 'supC':
                                {
                                    $Pdo->supC($_POST['NumC']);
                                    ?>
                                    <script >
                                        document.location.href="index.php?page=Controler&param=AjoutCour";
                                    </script>
                                    <?php
                                    break;
                                }
                            case 'supH':
                                {
                                    $Pdo->supH($_POST['NumH']);
                                    ?>
                                    <script >
                                        document.location.href="index.php?page=Controler&param=AjoutHoraire";
                                    </script>
                                    <?php
                                    break;
                                }
                            case 'supI':
                                {
                                    $Pdo->supI($_POST['NumI']);
                                    ?>
                                    <script >
                                        document.location.href="index.php?page=Controler&param=AjoutIntervenant";
                                    </script>
                                    <?php
                                    break;
                                }
                        }
                    }
                }
				break;
			}
            case 'Envoyer':
            {
                 Contact($_POST['Prenom_Nom'],$_POST['Email'],$_POST['Message'],$_POST['Sujet'],$_POST['Telephone']);
                ?>
                <script >
                    document.location.href="index.php?page=Controler&param=Contact";
                </script>
                <?php

                 break;
            }
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			case 'se-deconnecter' :
			{
				unset($_SESSION['NomAd']);
				unset($_SESSION['PrenomAd']);
				unset($_SESSION['DroitAd']);
				session_destroy();
				?>
					<script >
						document.location.href="index.php?page=Controler&param=Connexion";
					</script>
				<?php
				break;
			}
            case 'ajoutCHI':
            {
                $var=$Pdo->ajoutCHI($_POST['NumH'],$_POST['NumC'],$_POST['NumI']);
                ?>
                <script >
                    document.location.href="index.php?page=Controler&param=Cours";
                </script>
                <?php
                break;

            }
            case 'ajout_cour':
            {
                $var=$Pdo->ajoutC($_POST['NomC'],$_POST['Tarrif']);
                ?>
                <script >
                    document.location.href="index.php?page=Controler&param=AjoutCour";
                </script>
                <?php
                break;
            }
            case 'ajout_horaire':
            {
                echo  $_POST['DebH']."-".$_POST['FinH'];
                $var=$Pdo->ajoutH($_POST['DebH'],$_POST['FinH'],$_POST['LieuH'],$_POST['JourH']);
                ?>
                <script >
                    document.location.href="index.php?page=Controler&param=AjoutHoraire";
                </script>
                <?php
                break;
            }
            case 'ajout_intervenant':
                {
                    $var=$Pdo->ajoutI($_POST['NomI'],$_POST['PrenomI']);
                    ?>
                    <script >
                        document.location.href="index.php?page=Controler&param=AjoutIntervenant";
                    </script>
                    <?php
                    break;
                }
		}
	}
	?>


















