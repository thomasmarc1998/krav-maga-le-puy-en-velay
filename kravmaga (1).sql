-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Sam 01 Septembre 2018 à 11:47
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `kravmaga`
--

-- --------------------------------------------------------

--
-- Structure de la table `actualite`
--

CREATE TABLE `actualite` (
  `NumAct` int(11) NOT NULL,
  `Titre` varchar(100) DEFAULT NULL,
  `Description` varchar(256) DEFAULT NULL,
  `NomImage` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `actualite`
--

INSERT INTO `actualite` (`NumAct`, `Titre`, `Description`, `NomImage`) VALUES
(5, 'Professeur', 'ezlkjfoezfjizuhiui\'\'&quot;&quot;&quot;kjnjf', 'fichier_du_20180611084816.jpg'),
(6, 'iejfie', 'eq;jfhezy', 'fichier_du_20180615072656.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `adherent`
--

CREATE TABLE `adherent` (
  `NumAd` int(11) NOT NULL,
  `NomAd` varchar(100) DEFAULT NULL,
  `PrenomAd` varchar(100) DEFAULT NULL,
  `DroitAd` int(11) DEFAULT NULL,
  `PasseAd` varchar(100) DEFAULT NULL,
  `EmailAd` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `adherent`
--

INSERT INTO `adherent` (`NumAd`, `NomAd`, `PrenomAd`, `DroitAd`, `PasseAd`, `EmailAd`) VALUES
(1, 'Masson', 'Thomas', 1, '3c4a80dbdfac57d174d1cab8d11d03ad91888820', 'localhostdarko@gmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE `article` (
  `NumA` int(11) NOT NULL,
  `NomA` varchar(100) DEFAULT NULL,
  `Contenue` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `article`
--

INSERT INTO `article` (`NumA`, `NomA`, `Contenue`) VALUES
(5, NULL, '&lt;h2&gt;J\'en veux JOOOOOOOOOO&lt;/h2&gt;&lt;ol&gt;&lt;li&gt;&lt;strong&gt;dfdzf&lt;/strong&gt;&lt;/li&gt;&lt;/ol&gt;&lt;h4&gt;hghgqhshs&lt;/h4&gt;&lt;blockquote&gt;&lt;p&gt;kdjcjhjdhskchkdg&lt;/p&gt;&lt;p&gt;&amp;nbsp;&lt;/p&gt;&lt;p&gt;edef&lt;/p&gt;&lt;/blockquote&gt;'),
(6, NULL, '');

-- --------------------------------------------------------

--
-- Structure de la table `cours`
--

CREATE TABLE `cours` (
  `NumC` int(11) NOT NULL,
  `NomC` varchar(100) DEFAULT NULL,
  `Tarrif` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `cours`
--

INSERT INTO `cours` (`NumC`, `NomC`, `Tarrif`) VALUES
(1, 'Cours adultes', 200),
(2, 'Cours enfants', 120),
(4, 'Cours de taekwondo', 145);

-- --------------------------------------------------------

--
-- Structure de la table `courshorraire`
--

CREATE TABLE `courshorraire` (
  `NumC` int(11) NOT NULL,
  `NumH` int(11) NOT NULL,
  `NumI` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `courshorraire`
--

INSERT INTO `courshorraire` (`NumC`, `NumH`, `NumI`) VALUES
(1, 1, 1),
(1, 2, 1),
(2, 2, 1),
(1, 3, 1),
(2, 4, 1);

-- --------------------------------------------------------

--
-- Structure de la table `horaire`
--

CREATE TABLE `horaire` (
  `NumH` int(11) NOT NULL,
  `DebH` varchar(5) DEFAULT NULL,
  `FinH` varchar(5) DEFAULT NULL,
  `LieuH` varchar(100) NOT NULL,
  `JourH` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `horaire`
--

INSERT INTO `horaire` (`NumH`, `DebH`, `FinH`, `LieuH`, `JourH`) VALUES
(1, '19:00', '20:30', 'gymnase Marcel Pagnol', 'Mardi'),
(2, '12:20', '13:20', 'dojo stade massot ', 'jeudi'),
(3, '19:00', '20:30', 'dojo Mugamae ', 'Vendredi'),
(4, '17:45', '18:45', 'Dojo Mugamae', 'Vendredi'),
(5, '14:00', '15:00', 'Boulevard du test', 'Jeudi'),
(6, '18:00', '22:00', 'je sais pas germain!', 'Vendredi');

-- --------------------------------------------------------

--
-- Structure de la table `intervenant`
--

CREATE TABLE `intervenant` (
  `NumI` int(11) NOT NULL,
  `NomI` varchar(100) DEFAULT NULL,
  `PrenomI` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `intervenant`
--

INSERT INTO `intervenant` (`NumI`, `NomI`, `PrenomI`) VALUES
(1, 'Hugon', 'Hervé'),
(3, 'Masson', 'Thomas');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `actualite`
--
ALTER TABLE `actualite`
  ADD PRIMARY KEY (`NumAct`);

--
-- Index pour la table `adherent`
--
ALTER TABLE `adherent`
  ADD PRIMARY KEY (`NumAd`);

--
-- Index pour la table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`NumA`);

--
-- Index pour la table `cours`
--
ALTER TABLE `cours`
  ADD PRIMARY KEY (`NumC`);

--
-- Index pour la table `courshorraire`
--
ALTER TABLE `courshorraire`
  ADD PRIMARY KEY (`NumH`,`NumC`),
  ADD KEY `coursHorraire_ibfk_2` (`NumC`),
  ADD KEY `intervenant_ibfk_3` (`NumI`);

--
-- Index pour la table `horaire`
--
ALTER TABLE `horaire`
  ADD PRIMARY KEY (`NumH`);

--
-- Index pour la table `intervenant`
--
ALTER TABLE `intervenant`
  ADD PRIMARY KEY (`NumI`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `actualite`
--
ALTER TABLE `actualite`
  MODIFY `NumAct` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `adherent`
--
ALTER TABLE `adherent`
  MODIFY `NumAd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `article`
--
ALTER TABLE `article`
  MODIFY `NumA` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `cours`
--
ALTER TABLE `cours`
  MODIFY `NumC` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `horaire`
--
ALTER TABLE `horaire`
  MODIFY `NumH` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT pour la table `intervenant`
--
ALTER TABLE `intervenant`
  MODIFY `NumI` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `article_ibfk_1` FOREIGN KEY (`NumA`) REFERENCES `actualite` (`NumAct`);

--
-- Contraintes pour la table `courshorraire`
--
ALTER TABLE `courshorraire`
  ADD CONSTRAINT `coursHorraire_ibfk_1` FOREIGN KEY (`NumH`) REFERENCES `horaire` (`NumH`),
  ADD CONSTRAINT `coursHorraire_ibfk_2` FOREIGN KEY (`NumC`) REFERENCES `cours` (`NumC`),
  ADD CONSTRAINT `intervenant_ibfk_3` FOREIGN KEY (`NumI`) REFERENCES `intervenant` (`NumI`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
