<section class="page-wrap">
    <h1 class="center">Envoyer un message</h1>
    <div class="d-flex flex-row justify-content-center">
        <form enctype="multipart/form-data" method="POST" action="index.php?page=Controler&param=Envoyer">
            <div class="form-group">
                <input type="text" class="MonForm"  aria-describedby="emailHelp" placeholder="Prénom/Nom*" name="Prenom_Nom">
            </div>
            <div class="form-group"style="width: 300px;">
                <input type="text" class="MonForm"  placeholder="Téléphone*" name="Telephone">
            </div>
            <div class="form-group"style="width: 300px;">
                <input type="Email" class="MonForm"  placeholder="Email*" name="Email">
            </div>
            <div class="form-group"style="width: 300px;">
                <input type="Text" class="MonForm"  placeholder="Sujet*" name="Sujet">
            </div>
            <div class="form-group"style="width: 300px;">
                <textarea class="MonForm"  placeholder="Sujet*" rows="5" name="Message"></textarea>
            </div>
            <button type="submit" class="btn btn-outline-danger marginAuto w-100" value="envoyer">Envoyer</button>
        </form>
    </div>
</section>