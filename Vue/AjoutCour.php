<?php
if((isset($_SESSION['EmailAd'])&&!empty($_SESSION['EmailAd']))&&(isset($_SESSION['PasseAd'])&&!empty($_SESSION['PasseAd'])))
    {
        ?>
        <section class="page-wrap w-50" style="margin: auto">
            <h1 class="text-center">AJouter un Cours</h1>
            <form enctype="multipart/form-data" method="POST" action="index.php?page=Controler&param=ajout_cour">
                  <div class="form-group text-center mx-auto w-25">
                      <label for="formGroupExampleInput2">Nom du Cours</label>
                      <input type="text " class="MonForm text-center" required id="formGroupExampleInput2"  name="NomC">
                  </div>
                  <div class="form-group text-center mx-auto w-25">
                      <label for="formGroupExampleInput2">tarif</label>
                      <input type="text " class="MonForm text-center" required id="formGroupExampleInput2"  name="Tarrif">
                  </div>
                  <div class="form-group text-center mx-auto w-25">
                      <button type="submit" class="btn btn-outline-danger">Ok</button>
                  </div>
            </form>
            <h1 class="text-center">Suprimer ou Modifier</h1>
            <div class="table-responsive">
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col" style="width: 32px; ">Id</th>
                        <th scope="col" style="width: 200px; text-align: center">Cours</th>
                        <th scope="col" style="width: 200px; text-align: center">tarrif</th>
                        <th scope="col" style="width: 64px;">Modifier</th>
                        <th scope="col" style="width: 64px;">Suprimer</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($var as $ligne)
                    {
                        ?>
                        <tr>
                            <th  ><?php echo $ligne['NumC'];?></th>
                            <form enctype="multipart/form-data" method="POST" action="index.php?page=Controler&param=Modifier&modif=majC">
                                <input type="hidden" name="NumC" value="<?php echo $ligne['NumC'];?>">
                                <td  ><input type="text " class="MonForm text-center" value="<?php echo $ligne['NomC'];?>" required id="formGroupExampleInput2"  name="NomC"></td>
                                <td  ><input type="text " class="MonForm text-center" value="<?php echo $ligne['Tarrif'];?>" required id="formGroupExampleInput2"  name="Tarrif"></td>
                                <td ><input class="py-2" type="image"  src='Images/Icons/edit.png' style=" width: 32px ;cursor: pointer; outline: none;"></td>
                            </form>
                            <td>
                                <form enctype="multipart/form-data" method="POST" action="index.php?page=Controler&param=suprimer&sup=supC">
                                    <input type="hidden" name="NumC" value="<?php echo $ligne['NumC'];?>">
                                    <input class="py-2" type="image"  src='Images/Icons/garbage.png' style="width: 32px ; cursor: pointer; outline: none;">
                                </form>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                    <?php
                    ?>
                </table>
            </div>
        </section>
        <?php
    }
?>
