<?php
if((isset($_SESSION['EmailAd'])&&!empty($_SESSION['EmailAd']))&&(isset($_SESSION['PasseAd'])&&!empty($_SESSION['PasseAd'])))
    {
        ?>
        <form enctype="multipart/form-data" method="POST" action="index.php?page=Controler&param=ajout_horaire">
            <h2 class="text-center">Ajouter un horaire</h2>
            <div class="form-group text-center mx-auto w-25">
                <label for="formGroupExampleInput2">Début du cours à:</label>
                <input required type="time" class="MonForm"  name="DebH">
            </div>
            <div class="form-group text-center mx-auto w-25">
                <label for="formGroupExampleInput2">Fin du cours à:</label>
                <input required type="time" class="MonForm"  name="FinH">
            </div>
            <div class="form-group text-center mx-auto w-25">
                <label for="formGroupExampleInput2">lieu</label>
                <input required type="text " class="MonForm text-center" required id="formGroupExampleInput2" placeholder="..." name="LieuH">
            </div>
            <div class="form-group text-center mx-auto w-25 ">
                <label for="exampleFormControlSelect1">Jour</label>
                <select required class="MonForm" name ="JourH" class="form-control" id="exampleFormControlSelect1">
                    <option class="MonForm" value="Lundi">Lundi</option>
                    <option class="MonForm" value="Mardi">Mardi</option>
                    <option class="MonForm" value="Mercredi">Mercredi</option>
                    <option class="MonForm" value="Jeudi">jeudi</option>
                    <option class="MonForm" value="Vendredi">Vendredi</option>
                    <option class="MonForm" value="Samedi">Samedi</option>
                    <option class="MonForm" value="Dimanche">Dimanche</option>
                </select>
            </div>
            <div class="form-group text-center mx-auto w-25">
                <button type="submit" class="btn btn-outline-danger w-25">Ok</button>
            </div>
        </form>
        <h1 class="text-center">Suprimer ou Modifier</h1>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col" style="width: 32px;">Id</th>
                    <th scope="col" style="width: 150px; text-align: center">Début du cours à</th>
                    <th scope="col" style="width: 150px; text-align: center">Fin du cours à:</th>
                    <th scope="col" style="width: 150px; text-align: center">Lieu</th>
                    <th scope="col" style="width: 150px; text-align: center">Jour</th>
                    <th scope="col" style="width: 64px; ">Modifier</th>
                    <th scope="col" style="width: 64px;">Suprimer</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($var as $ligne)
                {
                    ?>
                    <tr>
                        <th style="padding: 20px;"><?php echo $ligne['NumH'];?></th>
                        <form enctype="multipart/form-data" method="POST" action="index.php?page=Controler&param=Modifier&modif=majH">
                            <input type="hidden" name="NumH" value="<?php echo $ligne['NumH'];?>">
                            <td><input type="time " class="MonForm text-center" value="<?php echo $ligne['DebH'];?>" required id="formGroupExampleInput2"  name="DebH"></td>
                            <td><input type="time " class="MonForm text-center" value="<?php echo $ligne['FinH'];?>" required id="formGroupExampleInput2"  name="FinH"></td>
                            <td><input type="text " class="MonForm text-center" value="<?php echo $ligne['LieuH'];?>" required id="formGroupExampleInput2"  name="LieuH"></td>
                            <td>
                                <select required class="MonForm " name ="JourH"  class="form-control" id="formGroupExampleInput2">
                                    <option class="MonForm text-center" value="<?php echo $ligne['JourH'];?>"><?php echo $ligne['JourH'];?></option>
                                    <option class="MonForm text-center" value="Lundi">Lundi</option>
                                    <option class="MonForm text-center" value="Mardi">Mardi</option>
                                    <option class="MonForm text-center" value="Mercredi">Mercredi</option>
                                    <option class="MonForm text-center" value="Jeudi">jeudi</option>
                                    <option class="MonForm text-center" value="Vendredi">Vendredi</option>
                                    <option class="MonForm text-center" value="Samedi">Samedi</option>
                                    <option class="MonForm text-center" value="Dimanche">Dimanche</option>
                                </select>
                            </td>
                            <td ><input class="py-2" type="image"  src='Images/Icons/edit.png' style=" width: 32px ;cursor: pointer; outline: none;"></td>
                        </form>
                        <td style="padding: 20px;"
                        <td>
                            <form enctype="multipart/form-data" method="POST" action="index.php?page=Controler&param=suprimer&sup=supH">
                                <input type="hidden" name="NumH" value="<?php echo $ligne['NumH'];?>">
                                <input type="image" src='Images/Icons/garbage.png' style="width: 25px; height: 25px; cursor: pointer; outline: none;">
                            </form>
                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
                <?php
                ?>
            </table>
        </div>
        <?php
    }
?>
