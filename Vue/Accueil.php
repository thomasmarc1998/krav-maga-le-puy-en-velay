<section class="page-wrap">
	<div class="topnav">
	  <a class="active" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation" href="#home">Ajouter Actu</a>
	  <a href="#about">Plus récents</a>
	  <a href="#contact">plus vieux</a>
	  <div class="search-container">
	    <form action="/action_page.php">
	      <input  class ="recherche" type="text" placeholder="Recherche.." name="search">
	      <button type="submit"><i class="fa fa-search"></i></button>
	    </form>
	  </div>
	</div>
	<?php
		if((isset($_SESSION['EmailAd'])&&!empty($_SESSION['EmailAd']))&&(isset($_SESSION['PasseAd'])&&!empty($_SESSION['PasseAd'])))
		{
			?>
				<div  class="bg-light collapse navbar-collapse" id="navbarTogglerDemo02">
                    <form enctype="multipart/form-data" method="POST" action="index.php?page=Controler&param=Ajouter" class="mx-auto">
                        <div class="form-group w-25 mx-auto text-center">
                            <h2 class="center">Ajouter atcualite</h2>
                            <input type="hidden" name="MAX_FILE_SIZE" required value="10000000"  />
                            <input type="file" name="Image" id="file-200" class="inputfile inputfile-200" data-multiple-caption="{count} files selected" multiple required />
                            <label for="file-200"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choisir une image&hellip;</span></label>
                        </div>
                        <div class="form-group text-center mx-auto w-25">
                            <label for="formGroupExampleInput2">Choisir un titre</label>
                            <input type="text " class="MonForm text-center" required id="formGroupExampleInput2" placeholder="..." name="Titre">
                        </div>
                        <div class="form-group text-center mx-auto w-25">
                            <label for="exampleFormControlTextarea1">Choisir la description</label>
                            <textarea class="MonForm" id="exampleFormControlTextarea1" required rows="3" placeholder="..." name="Description"></textarea>
                        </div>
                        <div class="d-flex justify-content-center mx-auto w-25">
                            <button type="submit" class="btn btn-outline-danger marginAuto w-100" >Ajouter</button>
                        </div>
                    </form>
				</div>
			<?php
		}
	?>
	<h1 class="center">!!!SITE EN CONSTRUCTON ET EN PHASE DE TEST!!!</h1>
	<div class="d-flex flex-row  flex-wrap justify-content-center">
		<?php foreach($var as $ligne)
			{
				   if((!isset($_SESSION['EmailAd'])&&empty($_SESSION['EmailAd']))&&(!isset($_SESSION['PasseAd'])&&empty($_SESSION['PasseAd'])))//Si on nest oas Admin
				   {
					   ?>
						   <div class="d-flex justify-content-center">
								<div class="carte" style="width: 18rem;">
								  <div class="card-body">
                                   <img src="Images/Actualite/<?php echo $ligne['NomImage']?>" style=" height: 17rem; " class="img-fluid mb-4" alt="Responsive image">
								    <h2  class="card-title"><?php echo $ligne['Titre']?></h2>
								    <hr class="style1">
								    <p class="card-text"><?php echo $ligne['Description']?></p>
								    <div class="d-flex justify-content-center">
								    	 <a href="index?page=Controler&param=Article&id=<?php echo $ligne['NumA']?>" class="btn btn-outline-danger marginAuto w-100">Lire Plus</a>
								    </div>
								  </div>
								</div>
							</div>
						<?php
					}
					if((isset($_SESSION['EmailAd'])&&!empty($_SESSION['EmailAd']))&&(isset($_SESSION['PasseAd'])&&!empty($_SESSION['PasseAd'])))//Si on est Admin
					{
						?>
					<form enctype="multipart/form-data" method="POST" action="index.php?page=Controler&param=Modifier&modif=modifActu">
						 <div class="d-flex justify-content-center">
							<div class="carte" style="width: 18rem;">
								<div class="w-100 d-flex justify-content-end">
									<span class="close" onclick="location.href='index.php?page=Controler&param=suprimer&sup=supAct&NumAct=<?php echo $ligne['NumAct']?>'"></span>
								</div>
							  <div class="card-body">
                               <div class="form-group text-center d-flex justify-content-center flex-column">
                                  <img src="Images/Actualite/<?php echo $ligne['NomImage']?>" style="height: 15rem" class="img-fluid mb-4" alt="Responsive image">
                                  <input type="hidden" name="MAX_FILE_SIZE"  value="10000000"  />
                                  <input type="file" name="Image" id="file-<?php echo $ligne['NumAct']?>" class="inputfile inputfile-<?php echo $ligne['NumAct']?>" data-multiple-caption="{count} files selected" multiple  />
                                  <label for="file-<?php echo $ligne['NumAct']?>"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span> <?php echo $ligne['NomImage']?>&hellip;</span></label>
                               </div>
							    <div class="form-group text-center">
								    <label for="formGroupExampleInput2">Modifier Titre</label>
								    <input type="text " class="MonForm text-center" id="formGroupExampleInput2" name="Titre" value="<?php echo $ligne['Titre']?>">
							    </div>
							    <hr class="style1 w-100">
							    <div class="form-group text-center">
								    <label for="exampleFormControlTextarea1">Modifier Description</label>
								    <textarea class="MonForm" id="exampleFormControlTextarea1" name="Description"  rows="3"><?php echo $ligne['Description']?></textarea>
							 	</div>
							 	<input type="hidden" name="NumAct" value="<?php echo $ligne['NumAct']?>">
							    <div class="d-flex justify-content-center">
							    	<button type="submit" class="btn btn-outline-danger marginAuto w-100" >Modifier</button>
							    </div>
                               <div class="d-flex justify-content-center">
                                   <a href="index?page=Controler&param=Article&id=<?php echo $ligne['NumA']?>" class="btn btn-outline-danger marginAuto w-100">Lire Plus</a>
                               </div>
							  </div>
							</div>
						</div>
					</form>
				<?php
				}
			}
		?>
	</div>
</section>
<script type="text/javascript">
	'use strict';
	;( function ( document, window, index )
	{
		var inputs = document.querySelectorAll( '.inputfile' );
		Array.prototype.forEach.call( inputs, function( input )
		{
			var label	 = input.nextElementSibling,
				labelVal = label.innerHTML;

			input.addEventListener( 'change', function( e )
			{
				var fileName = '';
				if( this.files && this.files.length > 1 )
					fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
				else
					fileName = e.target.value.split( '\\' ).pop();

				if( fileName )
					label.querySelector( 'span' ).innerHTML = fileName;
				else
					label.innerHTML = labelVal;
			});

			// Firefox bug fix
			input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
			input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
		});
	}( document, window, 0 ));
</script>
<script type="text/javascript">
'use strict';
	;( function( $, window, document, undefined )
	{
		$( '.inputfile' ).each( function()
		{
			var $input	 = $( this ),
				$label	 = $input.next( 'label' ),
				labelVal = $label.html();

			$input.on( 'change', function( e )
			{
				var fileName = '';

				if( this.files && this.files.length > 1 )
					fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
				else if( e.target.value )
					fileName = e.target.value.split( '\\' ).pop();

				if( fileName )
					$label.find( 'span' ).html( fileName );
				else
					$label.html( labelVal );
			});

			// Firefox bug fix
			$input
			.on( 'focus', function(){ $input.addClass( 'has-focus' ); })
			.on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
		});
	})( jQuery, window, document );
</script>
